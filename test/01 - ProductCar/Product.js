class Product {

    constructor(name, price){
        this.name = name
        this.price = price
    }
  
    GetName(){
        return this.name
    }
  
    GetPrice(){
        return this.price
    }

}

module.exports = Product