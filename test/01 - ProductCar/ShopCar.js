class ShopCar {

    constructor(){
        this.products = []
        this.lowerPrice = null
        this.higherPrice = null
    }
  
    Add(product){
      this.SetLowerProductPrice(product)
      this.SetHigherProductPrice(product)
      this.products.push(product)
    }
  
    AddRange(products){
      products.forEach(p => {
        this.Add(p)
      })
    }

    SetLowerProductPrice(product) {
        if(product.GetPrice() < (this.GetProductLowerPrice() || Number.MAX_SAFE_INTEGER) ) 
          this.lowerPrice = product
    }

    SetHigherProductPrice(product) {
        if(product.GetPrice() > (this.GetProductHigherPrice() || Number.MIN_SAFE_INTEGER) ) 
          this.higherPrice = product
    }
  
    GetLower(){
      return this.lowerPrice
    }
  
    GetProductLowerPrice(){
        return this.GetLower() && this.GetLower().GetPrice()
    }
  
    GetHigher(){
      return this.higherPrice
    }
  
    GetProductHigherPrice(){
        return this.GetHigher() && this.GetHigher().GetPrice()
    }
  
  }

module.exports = ShopCar