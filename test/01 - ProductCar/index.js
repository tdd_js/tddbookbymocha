const assert = require('assert')

const ShopCar = require('./ShopCar')
const Product = require('./Product')

describe('testOnlyOneProduct', () => {

  let shopCar = new ShopCar()

  shopCar.AddRange([
    new Product("Refrigerator", 450),
    new Product("TV", 800),
    new Product("Notebook", 1800),
    new Product("Mouse", 50),
    new Product("Guitar", 500),
  ])

  it('Product name with lower price...', () => {
    assert.equal('Mouse', shopCar.GetLower().GetName())
  })

  it('Product name with higher price...', () => {
    assert.equal('Notebook', shopCar.GetHigher().GetName())
  })

})