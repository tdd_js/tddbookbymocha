const RomanusSimbols = require('./RomanusSimbols')

module.exports = {

    ConvertSimbolsToNumbers(word) {
        let result = 0
        let lastNumber = 0
        word.split('').forEach(letter => {
            if(Object.keys(RomanusSimbols).find(l => letter === l)) {
                result += 
                    RomanusSimbols[letter] - (lastNumber >= RomanusSimbols[letter] ? 0 : lastNumber*2)
                
                lastNumber = RomanusSimbols[letter]
            }
        })
        
        return result
    }

}