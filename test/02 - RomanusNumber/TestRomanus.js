const assert = require('assert')

const RomanusNumbers = require('./RomanusSimbols/RomanusNumbers')

module.exports = {

    Verify(simbolsArray, txt) {

        const simbolsKeys = Object.keys(simbolsArray)
    
        simbolsKeys.forEach(r => {
            const describeTxt = [txt, r].join(" ")
            console.log(r, simbolsArray[r], RomanusNumbers.ConvertSimbolsToNumbers(r)
            )
            it(describeTxt, () => {
                assert.equal(simbolsArray[r], RomanusNumbers.ConvertSimbolsToNumbers(r))         
            }) 
    
        })
    
    }
    
}