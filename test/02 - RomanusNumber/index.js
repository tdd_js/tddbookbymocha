const romanusTestNumber = require('./tests/romanusTestNumber')

const romanusTestManySimbolsEasy = require('./tests/romanusTestManySimbolsEasy')

const romanusTestManySimbolsMinus = require('./tests/romanusTestManySimbolsMinus')

const TestRomanus = require('./TestRomanus')

describe('Romanus Numbers', () => {

    TestRomanus.Verify(romanusTestNumber, 'Understand the simbol')

    TestRomanus.Verify(romanusTestManySimbolsEasy, 'Understand the number with more simbols')

    TestRomanus.Verify(romanusTestManySimbolsMinus, 'Understand the number with more simbols:')

})