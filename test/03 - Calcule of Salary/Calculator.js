const SalaryRules = require('./Rules/SalaryRules')

module.exports = {

    Salary(employe){
        let result = employe.GetSalary()
        
        let floorValue = 0
        let currentRule = 
        SalaryRules[employe.GetNivel()]
            .find(salaryRule => {
                const ceilValue = salaryRule.ceil || Number.MAX_SAFE_INTEGER
                const isScopeRule = result >= floorValue && result < ceilValue
                floorValue = ceilValue
                return isScopeRule
            })

        result -= currentRule ? currentRule.desc * result : 0
        
        return result
    }

}