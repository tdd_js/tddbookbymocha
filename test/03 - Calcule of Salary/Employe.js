class Employe {

    constructor(name, salary, nivel){
        this.name = name
        this.salary = salary
        this.nivel = nivel
    }

    GetName() {
        return this.name
    }

    GetSalary() {
        return this.salary
    }

    GetNivel() {
        return this.nivel
    }
    
}

module.exports = Employe