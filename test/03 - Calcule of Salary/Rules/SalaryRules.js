const Nivel = require('./Nivel')

const SalaryRules = {
    [Nivel.Developper]:
    [
        {ceil: 3000, desc: .1}, 
        {desc: .2}
    ],

    [Nivel.DBA]:
    [
        {ceil: 2500, desc: .15}, 
        {desc: .25}
    ],

    [Nivel.Tester]: 
    [
        {ceil: 2500, desc: .15}, 
        {desc: .25}
    ],
}

module.exports = SalaryRules