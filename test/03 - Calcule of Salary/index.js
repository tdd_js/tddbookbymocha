const assert = require('assert')

const Nivel = require('./Rules/Nivel')
const Employe = require('./Employe')

const TestSalary = require('./TestSalary')

describe('Calcule Salary', () => {

    TestSalary.Create(
        'Developper with salary less than 3000', 
        new Employe("Zé", 1500, Nivel.Developper),
        1500 * .9
    )

    TestSalary.Create(
        'Developper with salary more than 3000',
        new Employe("Giovanni", 3100, Nivel.Developper), 
        3100 * .8
    )

    TestSalary.Create(
        'DBA with salary less than 1500',
        new Employe("Júnior", 1500, Nivel.DBA), 
        1500 * .85
    )

    TestSalary.Create(
        'Tester with salary less than 1500',
        new Employe("Tete Júnior", 1500, Nivel.Tester), 
        1500 * .85
    )

})